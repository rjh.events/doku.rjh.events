---
title: "How to: Set up NanoStation AC Loco as Wireless Access Point"
description: ""
summary: ""
date: 2023-11-05T13:55:25+01:00
lastmod: 2023-11-05T13:55:25+01:00
draft: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
## Before you begin

You should [download the newest firmware](https://www.ui.com/download/software/loco5ac) for your device, so you can upgrade it without connecting it to the internet.

### Optional

Unless your device is brand new, reset it by holding in the reset button for 10 seconds while the device is powered on.


## Initial Setup

Loco5AC:B4FBE40C05F2
<Model>:<MAC Address>

192.168.172.1

Select Country (Lie and get a fine)
Select Language
"I agree to the Ubiquiti Networks TERMS OF USE, EULA and PRIVACY POLICY"

Setup Admin User

## Recommended

Update Firmware

## Wireless

Wireless Mode: Station PtMP
SSID: <identity>_no-internet

## Network

### Network Role

Network Mode: Router

### Configuration Mode
Configuration Mode: Advanced

### WAN Network Settings

WAN Interface: LAN0
IPv6: Off
Block Management Access: Off

### LAN Network Settings

LAN Interface: BRIDGE0
IP Address: 10.0.0.1
Netmask: 255.255.255.0
DHCP Server: Enable
Range Start: 10.0.0.10
Range End: 10.0.0.100
Netmask: 255.255.255.0
Primary DNS: 10.0.0.1
Block Management Access: Off
IPv6: Off
