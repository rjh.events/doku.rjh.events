---
title: "How to Learn How to Mix"
description: ""
summary: ""
date: 2023-11-09T23:51:50+01:00
lastmod: 2023-11-09T23:51:50+01:00
draft: false
menu:
  docs:
    parent: ""
    identifier: "how-to-learn-how-to-mix-03dc89fd77f0fe280097eff9a2965463"
weight: 200
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
The goal of this tutorial is to teach you **how to teach yourself how to mix**, as opposed to teaching you how to mix.<br>
Mixing is a *skill* you build up over time; a toolbox you add tools to over many years. It is not a how-to you follow or a single technique you apply to make music sound good.<br>
Anyone who tells you otherwise is either a charlatan out for your money or time, or not worth listening to.

Mixing music can seem daunting. It is a craft with so many facets you will never run out of things to learn, but, luckily, you don't need to know everything to do it.

A basic understanding of loudness, equalization, compression and reverberation will allow you to take most raw audio and make it pleasent and clear to listen to, as well as bringing out the most important parts of the music.

We will go through each of these tools, one by one, explaining why they are needed, what you can achieve with them and how you can build up your understanding of them.

## Loudness

It's easier to hear something if it's loud. Everybody knows that.<br>
But *how loud should it be*?

Everything is there for a reason, bla  bla

## Equalization

## Compression

## Reverberation

