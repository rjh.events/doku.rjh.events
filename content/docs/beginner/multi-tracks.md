---
title: "Multi-tracks"
description: ""
summary: ""
date: 2023-11-07T10:47:51+01:00
lastmod: 2023-11-07T10:47:51+01:00
draft: false
menu:
  docs:
    parent: ""
    identifier: "multi-tracks-c78d5d5f9400d921d5c4ac28ea2c69a3"
weight: 999
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
When starting out can be the time in your carreer when feels the hardest to get any practice.

Practice using multi-tracks to get experience early on and make the most of the time you get to mix a live band.

## Free sources of multi-tracks

These are provided for educational purposes only. Use them to develop your skills, to get opportunities recording and mixing bands yourself.

Do not use multi-tracks found here for commercial purposes including a portfolio.

For more information on allowed usage, refer to the relevant source.

### Cambridge Music Technology

Big library of multi-tracks, in many musical genres.

[Cambridge Music Technology](https://cambridge-mt.com/ms/mtk/)

### Telefunken

Multi-tracks recorded using Telefunken microphones.

[Live From The Lab](https://www.telefunken-elektroakustik.com/livefromthelab/)

[Multitracks](https://www.telefunken-elektroakustik.com/multitracks/)

Despite the name, both links contain multi-tracks.
