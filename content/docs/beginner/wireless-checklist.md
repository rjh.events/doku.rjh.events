---
title: "Wireless Checklist"
description: ""
summary: ""
date: 2023-11-09T11:52:47+01:00
lastmod: 2023-11-09T11:52:47+01:00
draft: true
menu:
  docs:
    parent: ""
    identifier: "wireless-checklist-18864f965c1b66e296e021d4bc2e3429"
weight: 999
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
# TODO: Gather inspiration from similar checklists.
---
This is intended for small systems (1-2 transmitters/receivers) in (semi-)permanent applications, such as house systems in venues.

- [ ] TODO: Antenna placement
- [ ] Take note of [legal frequencies (DK)](https://frekvens.erhvervsstyrelsen.dk/) in your area.
- [ ] Use the Scan function on each transmitter/receiver to find channels with low interference.
  - [ ] Apply the frequency on each pair one at a time.
- [ ] Change the batteries before sound check.
- [ ] Change the batteries before the show.
