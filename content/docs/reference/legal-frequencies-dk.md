---
title: "Legal Frequencies in Denmark"
description: ""
summary: ""
date: 2023-11-07T10:25:49+01:00
lastmod: 2023-11-07T10:25:49+01:00
draft: false
menu:
  docs:
    parent: ""
    identifier: "legal-frequencies-dk-aa3be839c44f4de811d63f8388196605"
weight: 999
toc: true
seo:
  title: "" # custom title (optional)
  description: "" # custom description (recommended)
  canonical: "" # custom canonical URL (optional)
  noindex: false # false (default) or true
---
For an up to date list of legal frequencies for wireless equipment, refer to the [reference site of the danish authorities](https://frekvens.erhvervsstyrelsen.dk/).

They differ from location to location, so type in the address of your workplace or click on the map to select the location.
