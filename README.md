# Doku.RJH.Events

This site is built with the [Doks](https://getdoks.org/docs/start-here/getting-started/) theme for [Hyas](https://docs.gethyas.com/getting-started/).

Aims to follow [Diátaxis](https://diataxis.fr/).

## How to Create new Content

`npm run create docs/category/post.md`

## How to Update Doks

`npm install @hyas/doks-core@latest`

## How to run Testing Web Server

`npm run dev`

## How to Make a Category

## How to Make a Sub-Category

